#!/bin/bash
export EDITOR="/usr/bin/nano"
mkdir -p ~/.local/share/Wuerfel
mkdir -p ~/.local/bin
(
  echo 25
  echo "# Setting up..."
  echo "[Desktop Entry]
  Type=Application
  Encoding=UTF-8
  Name=Würfel
  Comment=Ein terminal würfel in java
  Exec=/bin/sh -c "$HOME/.local/bin/würfel"
  Icon=Wuerfel.png
  Terminal=true" > ~/.local/share/applications/Wuerfel.desktop
  sleep 2

  echo 30
  echo "# Reading files..."
  sleep 1

  echo 70
  echo "# Creating content..."
FILE=~/.local/share/Wuerfel/jdk-15.0.1/bin/javac
if [ ! -f "$FILE" ]; then 
    wget https://download.java.net/java/GA/jdk15.0.1/51f4f36ad4ef43e39d0dfdbaf6549e32/9/GPL/openjdk-15.0.1_linux-x64_bin.tar.gz
    tar -xvf openjdk-15.0.1_linux-x64_bin.tar.gz -C ~/.local/share/Wuerfel/
    rm openjdk-15.0.1_linux-x64_bin.tar.gz
fi
  cp -f Würfel.java ~/.local/share/Wuerfel/
  cp -f würfel ~/.local/bin/
  cp -f Wuerfel.png ~/.local/share/icons/
  ~/.local/share/Wuerfel/jdk-15.0.1/bin/javac ~/.local/share/Wuerfel/Würfel.java
  chmod +x ~/.local/bin/würfel
  sleep 2

  echo 100
  echo "# Done!"
) | zenity --title "Installation" --progress --auto-kill
if zenity --question --title "Start?" --text "Möchtest du das Programm starten?"
then
~/.local/bin/würfel
fi
exit
