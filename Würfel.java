import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Würfel {
    public static void main(String[] args) throws InterruptedException, IOException {
        if (Files.notExists(Path.of("./config.properties"))) {
            try (OutputStream output = new FileOutputStream("./config.properties")) {

                Properties prop = new Properties();

                // set the properties value
                prop.setProperty("erlaubte_seitenzahl", "2");
                prop.setProperty("minimaleseitenzahl", "6");

                // save properties to project root folder
                prop.store(output, null);

                System.out.println(prop);

            } catch (IOException io) {
                io.printStackTrace();
            }
        }
        GetPropertyValues properties = new GetPropertyValues();
        properties.getPropValues();
        System.out.print("\033[H\033[2J");
        System.out.flush();
        boolean restart = true;
        boolean wiederholung;
        boolean trysame;
        while (restart == true) {
            restart = false;
            trysame = true;
            try {
                Scanner eingabe = new Scanner(System.in);
                System.out.print("Wie viele seiten hat der Würfel? ");
                int seiten = eingabe.nextInt();
                if (seiten % properties.erlaubte_seitenzahlintern == 0) {
                    if (seiten > properties.minimaleseitenzahlintern) {
                        while (trysame == true) {
                            trysame = false;
                            System.out.println("Der Würfel ist auf " + (int) (Math.random() * seiten + 1) + " Gelandet");
                            Scanner scanner = new Scanner(System.in);
                            System.out.println();
                            System.out.print("Nochmahl? drücke J um eine neue zahl einzugeben \noder Enter um die selbe Nummer noch einmal zu nehmen: ");
                            String wiederholen = scanner.nextLine();
                            if (wiederholen.isEmpty()) {
                                trysame = true;
                                System.out.print("\033[H\033[2J");
                                System.out.flush();
                            } else {
                                wiederholung = nochmal(wiederholen);


                                if (wiederholung == true) {
                                    restart = true;
                                    TimeUnit.MICROSECONDS.sleep(500);
                                    System.out.print("\033[H\033[2J");
                                    System.out.flush();
                                } else
                                    return;
                            }
                        }
                    } else {
                        properties.minimaleseitenzahlintern++;
                        System.out.println("Ein Würfel hat nicht weniger als " + properties.minimaleseitenzahlintern + " seiten!");
                        properties.minimaleseitenzahlintern--;
                        restart = true;
                        clearScreen();
                    }
                } else {
                    System.out.println("Ein würfel hat nur gerade zahlen");
                    restart = true;
                    clearScreen();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (InputMismatchException r) {
                System.out.println("Eingabe muss eine zahl sein!");
                restart = true;
                clearScreen();
            } catch (ArithmeticException uff) {
                System.out.println("Ein fehler in der config datei ist aufgetreten lade standart werte");
                properties.erlaubte_seitenzahlintern = 2;
                properties.minimaleseitenzahlintern = 6;
                restart = true;
                clearScreen();
            }
        }
    }


    public static boolean nochmal(String x) {
        return x.charAt(0) == 'J' || x.charAt(0) == 'j';
    }

    public static void clearScreen() throws InterruptedException {
        System.out.println();
        System.out.print("Neustart in 3");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("\b2");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("\b1");
        TimeUnit.SECONDS.sleep(1);
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public static class GetPropertyValues {
        public int minimaleseitenzahlintern;
        InputStream inputStream;
        public int erlaubte_seitenzahlintern;

        public int getPropValues() throws IOException {

            try {
                Properties prop = new Properties();
                String propFileName = "config.properties";

                inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

                if (inputStream != null) {
                    prop.load(inputStream);
                } else {
                    throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
                }

                int erlaubte_seitenzahl = Integer.parseInt(prop.getProperty("erlaubte_seitenzahl"));
                int minimaleseitenzahl = Integer.parseInt(prop.getProperty("minimaleseitenzahl"));
                minimaleseitenzahl--;
                erlaubte_seitenzahlintern = erlaubte_seitenzahl;
                minimaleseitenzahlintern = minimaleseitenzahl;
            } catch (Exception e) {
                System.out.println("Exception: " + e);
            } finally {
                inputStream.close();
            }
            return erlaubte_seitenzahlintern;

        }
    }
}
